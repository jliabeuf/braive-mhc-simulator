<?php
// Thresholds
$thresholds = array(
    'depression' => array(
        'high' => isset( $_POST['threshold_depression_high'] ) ? filter_input( INPUT_POST, 'threshold_depression_high', FILTER_SANITIZE_NUMBER_INT ) : 10,
        'low' => isset( $_POST['threshold_depression_low'] ) ? filter_input( INPUT_POST, 'threshold_depression_low', FILTER_SANITIZE_NUMBER_INT ) : 7,
    ),
    'anxiety' => array(
        'high' => isset( $_POST['threshold_anxiety_high'] ) ? filter_input( INPUT_POST, 'threshold_anxiety_high', FILTER_SANITIZE_NUMBER_INT ) : 7,
        'low' => isset( $_POST['threshold_anxiety_low'] ) ? filter_input( INPUT_POST, 'threshold_anxiety_low', FILTER_SANITIZE_NUMBER_INT ) : 6,
    ),
    'stress' => array(
        'high' => isset( $_POST['threshold_stress_high'] ) ? filter_input( INPUT_POST, 'threshold_stress_high', FILTER_SANITIZE_NUMBER_INT ) : 9,
        'low' => isset( $_POST['threshold_stress_low'] ) ? filter_input( INPUT_POST, 'threshold_stress_low', FILTER_SANITIZE_NUMBER_INT ) : 8,
    ),
    'phq9' => array(
        'high' => isset( $_POST['threshold_phq9_high'] ) ? filter_input( INPUT_POST, 'threshold_phq9_high', FILTER_SANITIZE_NUMBER_INT ) : 15,
        'low' => isset( $_POST['threshold_phq9_low'] ) ? filter_input( INPUT_POST, 'threshold_phq9_low', FILTER_SANITIZE_NUMBER_INT ) : 8,
    ),
    'gad7' => array(
        'high' => isset( $_POST['threshold_gad7_high'] ) ? filter_input( INPUT_POST, 'threshold_gad7_high', FILTER_SANITIZE_NUMBER_INT ) : 15,
        'low' => isset( $_POST['threshold_gad7_low'] ) ? filter_input( INPUT_POST, 'threshold_gad7_low', FILTER_SANITIZE_NUMBER_INT ) : 10,
    ),
    'mini_spin' => array(
        'high' => isset( $_POST['threshold_mini_spin_high'] ) ? filter_input( INPUT_POST, 'threshold_mini_spin_high', FILTER_SANITIZE_NUMBER_INT ) : 7,
        'low' => isset( $_POST['threshold_mini_spin_low'] ) ? filter_input( INPUT_POST, 'threshold_mini_spin_low', FILTER_SANITIZE_NUMBER_INT ) : 6,
    ),
    'pa' => array(
        'high' => isset( $_POST['threshold_pa_high'] ) ? filter_input( INPUT_POST, 'threshold_pa_high', FILTER_SANITIZE_NUMBER_INT ) : 4,
        'low' => isset( $_POST['threshold_pa_low'] ) ? filter_input( INPUT_POST, 'threshold_pa_low', FILTER_SANITIZE_NUMBER_INT ) : 4,
    ),
);

include( 'includes/thresholds.php' );
include( 'includes/copy.php' );

// @var $depression_score int
$score_depression = filter_input( INPUT_GET, 'score_depression', FILTER_SANITIZE_NUMBER_INT );

// @var $anxiety_score int
$score_anxiety = filter_input( INPUT_GET, 'score_anxiety', FILTER_SANITIZE_NUMBER_INT );

// @var $stress_score int
$score_stress = filter_input( INPUT_GET, 'score_stress', FILTER_SANITIZE_NUMBER_INT );

// @var $severity string
// The severity is used to decide which copy to display in the MHC results.
$severity = 'none';

$suggested_program = array(); // If the results for depression, anxiety and stress are all below the threshold we can't customize the suggestion.

// Check for moderate score on DASS21
if ( $score_depression >= $thresholds['depression']['low'] || $score_anxiety >= $thresholds['anxiety']['low'] || $score_stress >= $thresholds['stress']['low'] ) {
    $severity = 'moderate';
}

if ( $score_anxiety > $thresholds['anxiety']['low'] ) {

    $gad7_score = filter_input( INPUT_GET, 'score_gad7', FILTER_SANITIZE_NUMBER_INT );
    $minispin_score = filter_input( INPUT_GET, 'score_mini_spin', FILTER_SANITIZE_NUMBER_INT );
    $pa_part_1 = $pa_total = 1;

    // Trigger GAD-7 questionnaire
    // Trigger Mini-SPIN questionnaire
    // Trigger PA questionnaire (part 1)
    // Ask extra question about IRL impact

    if ( $pa_part_1 > 0 ) {
        // Trigger long PA questionnaire
        $pa_part_2 = filter_input( INPUT_GET, 'score_pa', FILTER_SANITIZE_NUMBER_INT );
        $pa_total = $pa_total + $pa_part_2;
    }

    if ( $score_depression < $thresholds['depression']['low'] ) {

        // Suggest the non-mixed programs.
        // The non-mixed programs are for people with high anxiety and low depression.
        
        if ( isset( $pa_part_2 ) && $pa_total > $thresholds['pa']['low'] ) {

            if ( $minispin_score >= $thresholds['mini_spin']['low'] ) {
                $suggested_program[] = 'sad+pa'; // Mixed social anxiety and panic attack.
            }

            if ( $gad7_score >= $thresholds['gad7']['low'] ) {
                $suggested_program[] = 'gad+pa'; // Mixed general anxiety and panic attack.
            }

            if ( $gad7_score < $thresholds['gad7']['low'] && $minispin_score < $thresholds['mini_spin']['low'] ) {
                $suggested_program[] = 'pa'; // Panic attack.
            }

        } else {

            if ( $minispin_score >= $thresholds['mini_spin']['low'] ) {
                $suggested_program[] = 'sad'; // Social anxiety.
            }

            if ( $gad7_score > $thresholds['gad7']['low'] ) {
                $suggested_program[] = 'gad'; // General anxiety.
            }

        }

    } else {

        // Suggest the mixed programs.
        // The mixed programs are for people with high anxiety and also some level of depression.
        
        if ( $score_depression < $thresholds['depression']['high'] ) {

            // Lite versions.
            // The lite versions are suggested to people with only moderate depression. The lite programs are shorter.

            if ( $minispin_score >= $thresholds['mini_spin']['low'] ) {
                // @todo: Replace sad by msad-lite when the program is available.
                $suggested_program[] = 'sad'; // Mixed social anxiety and depression lite.
            }

            if ( $gad7_score > $thresholds['gad7']['low'] ) {
                // @todo: Replace madd by madd-lite when the program is available.
                $suggested_program[] = 'madd'; // Mixed generalized anxiety and depression lite.
            }

            if ( isset( $pa_part_2 ) && $pa_total > $thresholds['pa']['low'] ) {
                // @todo: Replace madd by mpad-lite when the program is available.
                $suggested_program[] = 'madd'; // Mixed panic and depression lite.
            }

        } else {

            // Full versions.
            // The full versions are suggested to people with severe depression. The full programs are longer.

            if ( $minispin_score > $thresholds['mini_spin']['low'] ) {
                $suggested_program[] = 'msad'; // Mixed social anxiety and depression.
            }

            if ( $gad7_score > $thresholds['gad7']['low'] ) {
                $suggested_program[] = 'madd'; // General anxiety.
            }

            if ( isset( $pa_part_2 ) && $pa_total > $thresholds['pa']['low'] ) {
                $suggested_program[] = 'mpad';
            }

        }

    }

    // Check if any of the additional questionnaires are above the severe threshold
    if ( $gad7_score >= $thresholds['gad7']['high'] || $minispin_score >= $thresholds['mini_spin']['high'] || $pa_part_2 >= $thresholds['pa']['high'] ) {
        $severity = 'severe';
    }

}

if ( $score_depression >= $thresholds['depression']['low'] ) {

    // Trigger PHQ-9 questionnaire
    $score_phq9 = filter_input( INPUT_GET, 'score_phq9', FILTER_SANITIZE_NUMBER_INT );

	// Check for severity first.
	if ( $score_phq9 >= $thresholds['phq9']['high'] ) {
		// Severe depression.
		$severity = 'severe';
	}

	// Define programs incompatibilities.
	$incompatibility_matrix = array(
		'depression' => array(
			// Full versions of the mixed programs.
			'msad',
			'madd',
			'mpad',
			// Lite versions of the mixed programs.
			'sad', // @todo: Replace sad by msad-lite when the program is available.
			/*'madd-lite', // @todo: Uncomment when madd-lite is available.*/
			/*'mpad-lite', // @todo: Uncomment when mpad-lite is available.*/
		),
	);

	// Check if there are any programs that are incompatible with the depression programs that have already been suggested.
	$incompatibilities = array_intersect( $incompatibility_matrix['depression'], $suggested_program );

	if ( empty( $incompatibilities ) ) {
		if ( $score_phq9 >= $thresholds['phq9']['high'] ) {
			$suggested_program[] = 'dep-severe';
		} else {
			$suggested_program[] = 'dep-normal';
		}
	}
}

if ( $score_stress > $thresholds['stress']['high'] && 'severe' !== $severity ) {
    $suggested_program[] = 'stress';
}

echo "<h1>Results</h1>";
echo "<ul>
    <li>Severity: $severity</li>
    <li>Scores
        <ul>
            <li>Depression: $score_depression</li>
            <li>Anxiety: $score_anxiety</li>
            <li>Stress: $score_stress</li>
        </ul>
    </li>
</ul>";

echo isset( $copy_course[ $suggested_program[ 0 ] ] ) && ! empty( $suggested_program ) ? $copy_course[ $suggested_program[ 0 ] ] : '';
echo isset( $copy_severity[ $severity ] ) ? '<p>' . $copy_severity[ $severity ] . '</p>' : '';

if ( count( $suggested_program ) > 1 ) {
    echo 'Your answers indicates that you would benefit from the tools in either of the programs recommended. You can therefor choose the one you prefer. To learn more about the different programs, click on the recommendations below.';
}

echo "<p>Disclaimer: The mental health check is not a diagnostic tool on it’s own, but designed to help identify and get an overview of the challenges you are facing. It is also how we are be able to suggest the program(s) that are most relevant to you.</p><p>You can also print the complete results of your MHC and bring it along to your appointment with a doctor or a mental health professional. The results will provide useful information that will make it easier for them to set a correct diagnosis and treatment plan.</p>";