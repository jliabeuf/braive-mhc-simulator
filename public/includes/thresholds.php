<p><a href="/">&larr; Back to scores</a></p>
<h1>Thresholds</h1>
<form action="" method="post">
    <table>
        <tr>
            <td>Depression</td>
            <td>High: <input type="text" name="threshold_depression_high" value="<?php echo $thresholds['depression']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_depression_low" value="<?php echo $thresholds['depression']['low']; ?>"></td>
        </tr>
        <tr>
            <td>Anxiety</td>
            <td>High: <input type="text" name="threshold_anxiety_high" value="<?php echo $thresholds['anxiety']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_anxiety_low" value="<?php echo $thresholds['anxiety']['low']; ?>"></td>
        </tr>
        <tr>
            <td>Stress</td>
            <td>High: <input type="text" name="threshold_stress_high" value="<?php echo $thresholds['stress']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_stress_low" value="<?php echo $thresholds['stress']['low']; ?>"></td>
        </tr>
        <tr>
            <td>PHQ9</td>
            <td>High: <input type="text" name="threshold_phq9_high" value="<?php echo $thresholds['phq9']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_phq9_low" value="" disabled="disabled"></td>
        </tr>
        <tr>
            <td>GAD7</td>
            <td>High: <input type="text" name="threshold_gad7_high" value="<?php echo $thresholds['gad7']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_gad7_low" value="<?php echo $thresholds['gad7']['low']; ?>"></td>
        </tr><tr>
            <td>Mini-SPIN</td>
            <td>High: <input type="text" name="threshold_mini_spin_high" value="<?php echo $thresholds['mini_spin']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_mini_spin_low" value="<?php echo $thresholds['mini_spin']['low']; ?>"></td>
        </tr><tr>
            <td>PA</td>
            <td>High: <input type="text" name="threshold_pa_high" value="<?php echo $thresholds['pa']['high']; ?>"></td>
            <td>Low: <input type="text" name="threshold_pa_low" value="<?php echo $thresholds['pa']['low']; ?>"></td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="submit" value="See Results">
            </td>
        </tr>
    </table>
</form>