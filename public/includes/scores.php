<h1>Scores</h1>
<form action="/" method="get">
    <table>
        <tr>
            <td>Depression</td>
            <td><input type="text" name="score_depression"></td>
        </tr>
        <tr>
            <td>Anxiety</td>
            <td><input type="text" name="score_anxiety"></td>
        </tr>
        <tr>
            <td>Stress</td>
            <td><input type="text" name="score_stress"></td>
        </tr>
        <tr>
            <td>PHQ9</td>
            <td><input type="text" name="score_phq9"></td>
        </tr>
        <tr>
            <td>GAD7</td>
            <td><input type="text" name="score_gad7"></td>
        </tr>
        <tr>
            <td>Mini SPIN</td>
            <td><input type="text" name="score_mini_spin"></td>
        </tr>
        <tr>
            <td>PA</td>
            <td><input type="text" name="score_pa"></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Try">
            </td>
        </tr>
    </table>
</form>